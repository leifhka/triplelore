# TripleLore

[TripleLore](https://leifhka.org/lore/triplelore/index.html) is a program for loading RDF into a triplestores using [Lore](https://gitlab.com/leifhka/lore), with
(partial) support for [OWL 2 RL reasoning](https://www.w3.org/TR/owl2-profiles/#OWL_2_RL), and [R2RML mappings](https://www.w3.org/TR/r2rml/) of the resulting database.
Triplelore also supports making [direct mappings](https://www.w3.org/TR/rdb-direct-mapping/) from relational tables to RDF.

For use and examples, see:

```
java -jar triplelore.jar --help
```

For more information about features, requirements and use, see the [TripleLore-page](https://leifhka.org/lore/triplelore/index.html).
