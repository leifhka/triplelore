import Dependencies._

ThisBuild / scalaVersion     := "2.13.10"
ThisBuild / version          := "0.4.1-SNAPSHOT"
ThisBuild / organization     := "org.leifhka.triplelore"
ThisBuild / organizationName := "triplelore"

scalacOptions += "-deprecation"

trapExit := false

assembly / assemblyMergeStrategy := {
  case "module-info.class"                                          => MergeStrategy.discard
  case PathList("javax", "ws", "rs", xs @ _*)                       => MergeStrategy.first
  case PathList("javax", "annotation", xs @ _*)                     => MergeStrategy.first
  case PathList("javax", "validation", xs @ _*)                     => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "Log.class"            => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "LogFactory.class"     => MergeStrategy.first
  case PathList(ps @ _*) if ps.last endsWith "r2rml.core.shacl.ttl" => MergeStrategy.first
  case PathList("META-INF", _*) => MergeStrategy.discard
  case x =>
    val oldStrategy = (assemblyMergeStrategy in assembly).value
    oldStrategy(x)
}

lazy val root = (project in file("."))
  .settings(
    Compile / scalaSource := baseDirectory.value / "src",
    Compile / packageBin /mainClass := Some("org.leifhka.triplelore.TripleLoreApp"),
    //Compile / packageBin /packageOptions += Package.ManifestAttributes("Multi-Release" -> "true"),
    Compile / run /mainClass := Some("org.leifhka.triplelore.TripleLoreApp"),
    name := "triplelore",
    assembly / assemblyJarName := "triplelore.jar",
    crossTarget := baseDirectory.value / "target",
    libraryDependencies ++= Seq(
        "info.picocli" % "picocli" % "4.7.0",
        "org.apache.jena" % "jena-core" % "4.7.0",
        "org.apache.jena" % "jena-arq" % "4.7.0",
        "org.postgresql" % "postgresql" % "42.2.18",
        "org.slf4j" % "slf4j-nop" % "1.7.36"
    )
  )
