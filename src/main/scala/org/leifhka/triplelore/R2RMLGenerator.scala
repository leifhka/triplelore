package org.leifhka.triplelore

import java.io.PrintStream
import java.sql.{Connection,ResultSet,Statement,SQLException}
import org.slf4j.Logger;

object R2RMLGenerator {

  def execute(out: PrintStream, conn: Connection): Unit = {

    val prefixes = getPrefixDefs(conn);
    val mappings = getMappings(conn);
    val mappingFile = prefixes + "\n" + mappings;
    out.println(mappingFile);
  }

  def getPrefixDefs(conn: Connection): String = {

    val prefixes = new StringBuilder();
    prefixes ++= "@prefix rr: <http://www.w3.org/ns/r2rml#> .\n";
    
    try {
      val stmt = conn.createStatement();
      val prefixRS = stmt.executeQuery("SELECT * FROM prefix;");

      while (prefixRS.next()) {
        prefixes ++= "@prefix " + prefixRS.getString(1) + ": <" + prefixRS.getString(2) + "> .\n";
      }
    } catch {
      case ex: SQLException => {
        System.err.println("Error when fetching prefixes from database: " + ex.getMessage());
        System.exit(1); // TODO: Remove?
      }
    }

    prefixes.toString();
  }

  def getMappings(conn: Connection): String = {

    val mappings = new StringBuilder();
        
    try {
      val stmt = conn.createStatement();
      val mappingsClasses = stmt.executeQuery("SELECT cname FROM triplelore_class AS sc;");
      val mappingsProperties = stmt.executeQuery("SELECT DISTINCT pname, range FROM triplelore_property;"); // DISTINCT in case of multiple rows (contains NULLs)

      while (mappingsClasses.next()) {
        val cm = makeClassMapping(mappingsClasses.getString(1));
        mappings ++= cm + "\n";
      }

      while (mappingsProperties.next()) {
        val pm = makePropertyMapping(mappingsProperties.getString(1), mappingsProperties.getString(2));
        mappings ++= pm + "\n";
      }
    } catch {
      case ex: SQLException => {
        System.err.println("Error when fetching mapping information from database: " + ex.getMessage());
        System.exit(1);
      }
    }

    mappings.toString();
  }

  def makeClassMapping(cls: String): String = {
    s"""
      | <${cls.replace(".", "_")}> a rr:TriplesMap;
      |    rr:subjectMap [ a rr:SubjectMap;
      |      rr:class ${cls.replace(".", ":")};
      |      rr:column "individual"
      |   ];
      |   rr:logicalTable [ rr:tableName "$cls" ] .
    """.stripMargin;
  }

  def makePropertyMapping(prop: String, range: String): String = {
    s"""
      |<${prop.replace(".", "_")}> a rr:TriplesMap;
      |  rr:subjectMap [
      |      rr:column "subject"
      |    ];
      |  rr:predicateObjectMap [ a rr:PredicateObjectMap ;
      |      rr:objectMap [ ${makeObjectMap(prop, range)} ] ;
      |      rr:predicate ${prop.replace(".", ":")}
      |    ];
      |  rr:logicalTable [ rr:tableName "$prop" ] .
    """.stripMargin
  }

  def makeObjectMap(prop: String, range: String): String = {
    if (range != null) {
      s"""
          a rr:ObjectMap, rr:TermMap ;
          rr:column "object" ;
          rr:termType rr:Literal ;
          rr:datatype <${range}>
        """.stripMargin
    } else {
      s"""
          a rr:ObjectMap, rr:TermMap ;
          rr:termType rr:IRI ;
          rr:column "object" ;
        """.stripMargin
    }
  }
}
