package org.leifhka.triplelore

import org.apache.jena.rdf.model.Resource

object Utils {
  
  def toRelationName(r: Resource): String = {
    
    // TODO: Handle qname is null
    val qname = r.getModel().qnameFor(r.getURI()).split(":")
    val schema = if (qname(0).isBlank()) "public" else qname(0)
    schema + "." + qname(1)
  }

  def getIdentifier(res:Resource): String = {

    if (res.getURI() != null)
      res.getURI()
    else
      LoreWriterSyntactic.BLANK_NODE_PREFIX + res.getId().getBlankNodeId().toString()
  }

  def toImplication(body: String, head: String, backward: Boolean): String = {

    if (backward) {
      head + " <- " + body + ";\n"
    } else {
      body + " -> " + head + ";\n"
    }
  }
}
