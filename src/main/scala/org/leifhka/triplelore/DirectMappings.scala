package org.leifhka.triplelore;

import java.sql.{Connection,ResultSet,Statement,SQLException}
import scala.util.{Try, Success, Failure, Using}
import scala.collection.mutable.{Buffer, ListBuffer, HashMap, Map}

object DirectMappings {

  class Relation (
    val schema: String, 
    val name: String, 
    val baseUri: String, 
    val prefix: String,
    val columns: List[String],
    val datatypes: List[String],
    val primaryKey: List[String],
    val foreignKeys: List[ForeignKey], // fk -> (prefix, schema, relation, fcols)
    val forward: Boolean
  ) {}

  class ForeignKey (
    val cols: List[String],
    val fBaseUri: String,
    val fSchema: String,
    val fName: String,
    val fCols: List[String]
  ) {}

  def makeMappings(connection: Connection): Try[List[String]] = {

    getMappedRelations(connection).map(relations => {
      val createPrefixes = makePrefixes(relations)
      val createRelations = makeRelations(relations)
      val createMappingRules = makeMappingRules(relations)

      List(
        "IMPORT 'https://leifhka.org/lore/library/prefix.lore';",
        "IMPORT 'https://leifhka.org/lore/library/common_prefixes.lore';",
        "IMPORT 'https://leifhka.org/lore/library/triplelore.lore';"
      ) ++ 
      createPrefixes ++ createRelations ++ createMappingRules
    })
  }

  def getMappedRelations(connection: Connection): Try[List[Relation]] = {

    Using(connection.createStatement()) { stmt => {
      val res = stmt.executeQuery(
        """SELECT DISTINCT rel_schema, rel_name, base_uri, prefix, forward_chaining
           FROM mappings.direct_mapping;"""
      )

      val out: Buffer[Relation] = new ListBuffer()

      while (res.next()) {
        val schema = res.getString(1); 
        val name = res.getString(2); 
        val baseUri = res.getString(3); 
        val prefix = res.getString(4); 
        val forward = res.getBoolean(5); 

        val (columns, datatypes) = getColumns(connection, schema, name).get;
        val primaryKey = getPrimaryKey(connection, schema, name).get;
        val foreignKeys = getForeignKeys(connection, schema, name).get; 

        out += new Relation(schema, name, baseUri, prefix, columns, datatypes, primaryKey, foreignKeys, forward)
      }

      out.toList
    }}
  }

  def getColumns(connection: Connection, schema: String, name: String): Try[(List[String], List[String])] = {
    val q = 
      """SELECT DISTINCT col_name, data_type
         FROM mappings.column_info
         WHERE rel_schema=? AND rel_name = ?;"""

    Using(connection.prepareStatement(q)) { stmt => {

      stmt.setString(1, schema)
      stmt.setString(2, name)
      val res = stmt.executeQuery()

      val cols: Buffer[String] = new ListBuffer()
      val datatypes: Buffer[String] = new ListBuffer()

      while (res.next()) {
        cols += res.getString(1)
        datatypes += res.getString(2)
      }
      (cols.toList, datatypes.toList)
    }}
  }

  def getPrimaryKey(connection: Connection, schema: String, name: String): Try[List[String]] = {
    val q = 
      """SELECT DISTINCT col_name
         FROM mappings.primary_key
         WHERE rel_schema=? AND rel_name = ?;"""

    Using(connection.prepareStatement(q)) { stmt => {

      stmt.setString(1, schema)
      stmt.setString(2, name)
      val res = stmt.executeQuery()

      val cols: Buffer[String] = new ListBuffer()

      while (res.next()) {
        cols += res.getString(1)
      }
      cols.toList
    }}
  }

  def getForeignKeys(connection: Connection, schema: String, name: String): Try[List[ForeignKey]] = { 
    val q = 
      """SELECT DISTINCT constraint_name, col_name, foreign_rel_schema, foreign_rel_name, base_uri, foreign_col_name
         FROM mappings.foreign_key AS fk
              JOIN mappings.direct_mapping AS dm
              USING (rel_schema, rel_name)
         WHERE rel_schema=? AND rel_name = ?;"""

    Using(connection.prepareStatement(q)) { stmt => {

      stmt.setString(1, schema)
      stmt.setString(2, name)
      val res = stmt.executeQuery()

      val fks: Map[(String, String, String, String), ListBuffer[(String, String)]] = new HashMap()

      while (res.next()) {
        val constraintName = res.getString(1)
        val colName = res.getString(2)
        val fSchema = res.getString(3)
        val fName = res.getString(4)
        val fBaseUri = res.getString(5)
        val fCol = res.getString(6)

        val mapKey = (constraintName, fBaseUri, fSchema, fName)
        val mapVal = (colName, fCol)
        fks.getOrElseUpdate(mapKey, new ListBuffer())
        fks.get(mapKey).get.addOne(mapVal)
      }
      fks.map{ case ((_, fBaseUri, fSchema, fName), colsNfCols) => {
        val cols = colsNfCols.map{ case (c, _) => c}.toList
        val fCols = colsNfCols.map{ case (_, c) => c}.toList
        new ForeignKey(cols, fBaseUri, fSchema, fName, fCols)
      }}.toList
    }}
  }

  def makePrefixes(relations: List[Relation]): List[String] = {
    relations.flatMap(r =>
        List(
          s"CREATE SCHEMA IF NOT EXISTS ${r.prefix};",
          s"prefix('${r.prefix}', '${r.baseUri}${r.schema}/');"
        )
    )
  }

  def makeRelations(relations: List[Relation]): List[String] =
    relations.flatMap(makeRelationsFor)

  def makeRelationsFor(rel: Relation): List[String] = {

    List(
      s"CREATE RELATION ${rel.prefix}.${rel.name}(individual text);",
      s"triplelore_class('${rel.prefix}.${rel.name}');"
    ) ++
    makeForeignKeyRelations(rel) ++ makeColumnRelations(rel);
  }

  def makeForeignKeyRelations(rel: Relation): List[String] = {
    rel.foreignKeys
      .flatMap(fk => 
          List(
            s"CREATE RELATION ${makeFKRelationName(rel, fk)}(subject text, object text);",
            s"triplelore_property('${makeFKRelationName(rel, fk)}', null);"
          )
      )
  }

  def makeColRelationName(rel: Relation, col: String): String =
    s"${rel.prefix}.${rel.name}_$col"

  def makeColumnRelations(rel: Relation): List[String] = {
    rel.columns
      .zip(rel.datatypes)
      .map{ case (col, dt) =>
          (makeColRelationName(rel, col), dt, LoreWriterSyntactic.postgresToXsdType.getOrElse(dt, "text"))
      }.flatMap{ case (relName, pgdt, xsddt) =>
          List(
            s"CREATE RELATION $relName(subject text, object $pgdt);",
            s"triplelore_property('$relName', '$xsddt');"
          )
      }
  }

  def makeFKRelationName(rel: Relation, fk: ForeignKey): String =
    s"${rel.prefix}.${rel.name}_ref_${fk.cols.mkString("_")}"

  def makeMappingRules(relations: List[Relation]): List[String] =
    relations.flatMap(makeMappingRules)

  def makeMappingRules(rel: Relation): List[String] = {

    val fwd = if (rel.forward) " FORWARD " else ""
    val pk = makePKExp(rel);

    List(s"""CREATE $fwd IMPLICATION ${rel.prefix}.${rel.name} AS
            |SELECT $pk FROM ${rel.schema}.${rel.name} AS t;""".stripMargin) ++
    rel.columns
      .map(col =>
          s"""CREATE $fwd IMPLICATION ${makeColRelationName(rel, col)} AS
             |SELECT $pk, $col FROM ${rel.schema}.${rel.name} AS t
             |WHERE $col IS NOT NULL;""".stripMargin) ++
    rel.foreignKeys
      .map(fk =>
          s"""CREATE $fwd IMPLICATION ${makeFKRelationName(rel, fk)} AS
             |SELECT $pk, ${makeFKExp(rel, fk)} FROM ${rel.schema}.${rel.name} AS t
             |WHERE ${fk.cols.map(c => s"$c IS NOT NULL").mkString(" AND ")};""".stripMargin
      )
  }

  def makePKExp(rel: Relation): String = {

    if (rel.primaryKey.isEmpty) { // Make blank node expression
      val colVals = rel.columns.map(c => s"$c::text").mkString(" || '|' || ")
      s"'http://leifhka.org/lore/blank/' || md5($colVals)::text"
    } else {
      val suffix = rel.primaryKey.map(c => s"'$c=' || $c::text").mkString(" || ")
      s"'${rel.baseUri}${rel.schema}/${rel.name}/' || $suffix"
    }
  }

  def makeFKExp(rel: Relation, fk: ForeignKey): String = {

    val suffix = fk.cols
      .zip(fk.fCols)
      .map{ case (c, f) => s"'$f=' || $c::text" }.mkString(" || ';' || ")

    s"'${fk.fBaseUri}${fk.fSchema}/${fk.fName}/' || $suffix"
  }
}
