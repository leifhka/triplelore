package org.leifhka.triplelore

import java.io.File
import java.io.PrintStream

import org.apache.jena.vocabulary.RDF
import org.apache.jena.rdf.model.{Model, ModelFactory, Statement}
import org.apache.jena.riot.RDFDataMgr

import scala.collection.immutable.{HashMap, List}
import scala.jdk.CollectionConverters._

object LoreWriter {

  // Contains Properties that cannot be represented using Lore
  // (as they do not have a well-defined range)
  val DROP_TRIPLES = List(
    RDF.first,
    RDF.rest,
    RDF.subject,
    RDF.predicate,
    RDF.`object`
  )

  def readModels(inputFiles: List[String], skipCheck: Boolean): Model = {

    val okErr: (List[String], List[String]) = inputFiles.partition(new File(_).isFile)

    if (!okErr._2.isEmpty) {
      okErr._2.foreach(err => System.err.println(s"[Error] File not found: $err"))
      System.exit(1);
    }

    val model = okErr._1.map (RDFDataMgr.loadModel _)
      .fold (ModelFactory.createDefaultModel()) ((m1, m2) => m1.add(m2))

    if (!skipCheck) ModelChecker.check(model)

    model
  }

  def transformWriteTo(inputFiles: List[String], skipCheck: Boolean, backwards: Boolean, out: PrintStream): Unit = {

    val model = LoreWriter.readModels(inputFiles, skipCheck)
    val stmts = writeOntology(model, backwards) ++ writeTripleStatements(model)
    stmts.foreach(out.println _)
  }

  def writePrefixes(model: Model): Seq[String] = {

    model.getNsPrefixMap.asScala.flatMap { case (k, v) => 
          if (!k.isBlank())
            List(
              LoreWriterSyntactic.PREFIX_RELATION + s"('$k', '$v');",
              s"CREATE SCHEMA IF NOT EXISTS $k;",
            )
          else
            List(s"${LoreWriterSyntactic.PREFIX_RELATION}('public', '$v');")
        }.toSeq
  } 

  def writeOntology(model: Model, backwards: Boolean): Seq[String] = {

    List(
      "IMPORT 'http://leifhka.org/lore/library/prefix.lore';",
      "IMPORT 'http://leifhka.org/lore/library/triplelore.lore';",
      "IMPORT 'http://leifhka.org/lore/library/base_rdfs.lore';"
    ) ++:
    writePrefixes(model) ++:
    LoreWriterSemantic.makeMappings(backwards).flatMap(_.execute(model))
  }

  def writeTripleStatements(model: Model): Seq[String] = {

    model.listStatements
      .filterDrop(stmt => DROP_TRIPLES.contains(stmt.getPredicate()))
      .mapWith(stmt =>
        if (stmt.getPredicate() == RDF.`type`)
          writeClassAssertion(stmt) // Write x rdf:type C as C(x) and not rdf.type(x, C)
        else
          writePropertyAssertion(stmt)
      ).asScala.toSeq
  }

  def writeClassAssertion(stmt: Statement): String = {
    val cls = Utils.toRelationName(stmt.getObject.asResource)
    val ind = Utils.getIdentifier(stmt.getSubject)
    s"$cls('$ind');"
  }

  def writePropertyAssertion(stmt: Statement): String = {
    val prop = Utils.toRelationName(stmt.getPredicate)
    val sub = Utils.getIdentifier(stmt.getSubject)

    val objNode = stmt.getObject
    var obj = if (objNode.isLiteral)
                objNode.asLiteral.getLexicalForm
              else
                Utils.getIdentifier(objNode.asResource)
    obj = obj.replaceAll("'", "''") // Need to replace ' (e.g. Haley's) with '' (e.g. Haley''s)
    s"$prop('$sub', '$obj');"
  }
}
