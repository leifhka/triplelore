package org.leifhka.triplelore

import org.apache.jena.rdf.model._
import org.apache.jena.vocabulary._

import scala.collection.immutable.HashMap

object LoreWriterSyntactic {

  val PREFIX_RELATION: String = "prefix"
  val BLANK_NODE_PREFIX: String = "http://leifhka.org/triplelore/blanknode/node_"

  val xsdToPostgresType = HashMap.from(List(
        //(RDF.langString, "text"), // TODO: support language tags
        (ModelFactory.createDefaultModel().createResource("http://www.opengis.net/ont/geosparql#wktLiteral"), "geometry"),
        (XSD.date, "date"),
        (XSD.dateTime, "timestamp"),
        (XSD.decimal, "numeric"),
        (XSD.gDay, "timestamp"),
        (XSD.gMonth, "timestamp"),
        (XSD.gMonthDay, "timestamp"),
        (XSD.gYear, "timestamp"),
        (XSD.gYearMonth, "timestamp"),
        (XSD.integer, "bigint"),
        (XSD.negativeInteger, "bigint"),
        (XSD.nonNegativeInteger, "bigint"),
        (XSD.nonPositiveInteger, "bigint"),
        (XSD.normalizedString, "text"),
        (XSD.positiveInteger, "bigint"),
        (XSD.QName, "text"),
        (XSD.time, "time"),
        (XSD.token, "text"),
        (XSD.unsignedByte, "bytea"),
        (XSD.unsignedInt, "bigint"),
        (XSD.unsignedLong, "bigint"),
        (XSD.unsignedShort, "bigint"),
        (XSD.xboolean, "boolean"),
        (XSD.xbyte, "bytea"),
        (XSD.xdouble, "double precision"),
        (XSD.xfloat, "real"),
        (XSD.xint, "int"),
        (XSD.xlong, "bigint"),
        (XSD.xshort, "smallint"),
        (XSD.xstring, "text")
      ))

  val postgresToXsdType = HashMap.from(List(
        ("geometry", ModelFactory.createDefaultModel().createResource("http://www.opengis.net/ont/geosparql#wktLiteral")),
        ("date", XSD.date),
        ("timestamp", XSD.dateTime),
        ("timestamp without time zone", XSD.dateTime),
        ("timestamp with time zone", XSD.dateTime),
        ("numeric", XSD.decimal),
        ("bigint", XSD.integer),
        ("time", XSD.time),
        ("time without time zone", XSD.time),
        ("time with time zone", XSD.time),
        ("bytea", XSD.xbyte),
        ("boolean", XSD.xboolean),
        ("double precision", XSD.xdouble),
        ("real", XSD.xfloat),
        ("int", XSD.xint),
        ("integer", XSD.xint),
        ("smallint", XSD.xshort),
        ("text", XSD.xstring),
        ("character varying", XSD.xstring)
      ))
} 
