package org.leifhka.triplelore

import org.apache.jena.query.{ResultSet, QueryExecution, QuerySolution, QueryFactory, QueryExecutionFactory}
import org.apache.jena.rdf.model.{Model, Resource}
import org.apache.jena.vocabulary.{RDF, RDFS, OWL2}
import scala.util.{Using, Failure, Success}

object ModelChecker {

  val drop_props = LoreWriter.DROP_TRIPLES ++ List(
    // Defined in imported base_rdfs.lore
    RDF.`type`,
    RDFS.label,
    RDFS.subClassOf,
    RDFS.range,
    RDFS.domain,
    RDFS.subPropertyOf,
    OWL2.equivalentProperty,
    OWL2.unionOf,
    OWL2.inverseOf,
    OWL2.intersectionOf,
    OWL2.propertyChainAxiom
  )


  val drop_filter = drop_props.fold("true") { (s, r) => s"?prop != <$r> && $s" }
  
  val noTypeErr = s"""
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 

    SELECT DISTINCT ?prop
    WHERE {
      [] ?prop [] .
      FILTER NOT EXISTS {
        { ?prop a owl:ObjectProperty }
        UNION
        { ?prop a owl:DatatypeProperty }
      }
      FILTER (${drop_filter})
    }
  """

  val noTypeErrMsg = "[INFO] All properties needs to be typed with either owl:ObjectProperty or owl:DatatypeProperty. The following properties did not have any such rdf:type defined in the input models (this may lead to an error unless these properties are already types in the target database):"

  val noRangeErr = s"""
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

    SELECT DISTINCT ?prop
    WHERE {
      ?prop a owl:DatatypeProperty .
      FILTER NOT EXISTS {
        ?prop rdfs:range [] .
      }
      FILTER (${drop_filter})
    }
  """

  val noRangeErrMsg = "[WARNING] All datatype properties needs to have a range (set via rdfs:range). The following datatype properties did not have any rdfs:range set in the input models (this may lead to an error unless these properties already have a range defined in the target database):"

  def check(model: Model): Unit = {
    checkFor(model, noTypeErr, noTypeErrMsg)
    checkFor(model, noRangeErr, noRangeErrMsg)
    checkForMissingQnames(model)
  }

  def withResultsOf(model: Model, query: String, apply: ResultSet => Unit): Unit = {

    val sq = QueryFactory.create(query)

    Using.resource(QueryExecutionFactory.create(sq, model)) { qexec =>

      val res = qexec.execSelect()

      if (res.hasNext()) {
        apply(res);
      }
    }
  }

  def checkFor(model: Model, query: String, msg: String): Unit = {
    
    withResultsOf(model, query, res => {
        System.err.println(msg)
        while (res.hasNext()) {
          System.err.println(s"    <${res.next().getResource("prop").toString()}>")
        }
      }
    );
  }

  def checkForMissingQnames(model: Model): Unit = {

    // Finds all classes
    val q = """
      PREFIX owl: <http://www.w3.org/2002/07/owl#> 
      PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>

      SELECT DISTINCT ?r
      WHERE { { ?r a rdfs:Class . } UNION
              { ?r a owl:Class . } UNION
              { [] a ?r . } UNION
              { [] rdfs:subClassOf ?r . } UNION
              { ?r rdfs:subClassOf [] . } UNION
              { [] rdfs:range ?r . } UNION
              { [] rdfs:domain ?r . }  UNION
              { ?r a owl:ObjectProperty . } UNION
              { ?r a owl:DatatypeProperty . }
              FILTER (!isBlank(?r)) }"""

    withResultsOf(model, q, res => {
        var msgPrinted = false;
        val msg = "[ERROR] Triplelore requires that all classes and properties must be written as QNames (i.e. prefix + local name). The following classes and/or properties are not written as QNames:";

        while (res.hasNext()) {
          val r = res.next().getResource("r").getURI();
          if (model.qnameFor(r) == null) {
            if (!msgPrinted) {
              System.err.println(msg);
              msgPrinted = true;
            }
            System.err.println(s"    <${r.toString()}>");
          }
        }

        if (msgPrinted) {
          System.exit(1);
        }
      }
    );
  }
}
