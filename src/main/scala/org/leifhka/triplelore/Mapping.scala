package org.leifhka.triplelore

import org.apache.jena.query.{ResultSet, QueryExecution, QuerySolution, QueryFactory, QueryExecutionFactory}
import org.apache.jena.rdf.model.{Resource, Model}

import scala.jdk.CollectionConverters._
import scala.util.Using

class Mapping(query:String, toLore: (ResultSet => Seq[String])) {

  def execute(model: Model): Seq[String] = {

    val q = Mapping.PREFIXES + "\n" + this.query
    val sq = QueryFactory.create(q)

    Using.resource(QueryExecutionFactory.create(sq, model)) { qexec =>
      this.toLore(qexec.execSelect())
    }
  }
}

object Mapping {

  val PREFIXES: String = """
    PREFIX owl: <http://www.w3.org/2002/07/owl#> 
    PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> 
    PREFIX xsd: <http://www.w3.org/2001/XMLSchema#> 
    PREFIX rdfs: <http://www.w3.org/2000/01/rdf-schema#>""" 

  def apply(query:String, toLore: (ResultSet => Seq[String])): Mapping =
    new Mapping(query, toLore);
    
  def makeClassMapping(query: String): Mapping =
    Mapping(query, toResFun(toLoreClassFunction))

  def makeObjectPropertyMapping(query: String): Mapping =
    Mapping(query, toResFun(toLoreObjectPropertyFunction))

  def makeDatatypePropertyMapping(query: String): Mapping =
    Mapping(query, toResFun(toLoreDatatypePropertyFunction))

  def makeRuleMapping(query: String, body: String, head: String, backwards: Boolean): Mapping =
    Mapping(query, toLoreRuleFunctionSimple(body, head, backwards))

  def toResFun(f: (QuerySolution => Seq[String])): (ResultSet => Seq[String]) =
    res => {
      var stmts: List[String] = List()
      for (sol <- res.asScala)
        stmts = stmts ++ f(sol)
      stmts
    }
  
  def toLoreObjectPropertyFunction(sol: QuerySolution): Seq[String] = {

    val prop = Utils.toRelationName(sol.getResource("p"));

    List(
      s"CREATE RELATION $prop(subject text, object text);",
      s"triplelore_property('$prop', NULL);"
    )
  }

  def toLoreDatatypePropertyFunction(sol: QuerySolution): Seq[String] = {

    val prop = Utils.toRelationName(sol.getResource("p"));
    val rangeRes = sol.getResource("r")
    val spRange = rangeRes.getURI()
    val pgRange = LoreWriterSyntactic.xsdToPostgresType.getOrElse(rangeRes, "text");

    List(
      s"CREATE RELATION $prop(subject text, object $pgRange);",
      s"triplelore_property('$prop', '$spRange');" 
    )
  }

  def toLoreClassFunction(sol: QuerySolution): Seq[String] = {

    val cls = Utils.toRelationName(sol.getResource("c"));

    List(
      s"CREATE RELATION $cls(individual text);",
      s"triplelore_class('$cls');"
    )
  }

  def toLoreRuleFunctionSimple(body: String, head: String, backwards: Boolean): (ResultSet => Seq[String]) =
    toLoreRuleFunction(Utils.toRelationName, body, head, backwards)

  def toLoreRuleFunction(bodyFn: (Resource => String), body: String, head: String, backwards: Boolean): (ResultSet => Seq[String]) =
    res => {
      var stmts: List[String] = List()
      val numBodyVars = res.getResultVars().size() - 1

      for (sol <- res.asScala) {

        val bodyVals = for(v <- 1 to numBodyVars)
          yield bodyFn.apply(sol.getResource("b" + v))

        val actHead = head.format(Utils.toRelationName(sol.getResource("h")))
        val actBody = body.format(bodyVals: _*)
        val impl = Utils.toImplication(actBody, actHead, backwards)
        stmts = stmts :+ impl
      }
      stmts
    }
}
