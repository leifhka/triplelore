package org.leifhka.triplelore

import org.apache.jena.rdf.model.{Resource, RDFList}
import scala.jdk.CollectionConverters._

object LoreWriterSemantic {

  // Classes: Need to always use ?c as class variable
  // ObjectProperties: Need to always use ?p as property variable
  // DatatypeProperty: Need to always use ?p as property variable and ?r as range
  // Rules: Need to alwaus use ?h for head-relation and ?bN for each body relation
  def makeMappings(backwards: Boolean): Seq[Mapping] = List(
    Mapping.makeClassMapping( // CLASSES
      """SELECT DISTINCT ?c
         WHERE { { ?c rdf:type rdfs:Class . } UNION
                 { ?c rdf:type owl:Class . } UNION
                 { [] rdf:type ?c . } UNION
                 { [] rdfs:subClassOf ?c . } UNION
                 { ?c rdfs:subClassOf [] . } UNION
                 { [] rdfs:range ?c . } UNION
                 { [] rdfs:domain ?c . } 
                 FILTER (!isBlank(?c)) }"""
    ),
    Mapping.makeObjectPropertyMapping( // OBJECT PROPERTIES
      "SELECT DISTINCT ?p WHERE { ?p rdf:type owl:ObjectProperty . }"
    ),
    Mapping.makeDatatypePropertyMapping( // DATATYPE PROPERTIES
      "SELECT DISTINCT ?p ?r WHERE { ?p rdf:type owl:DatatypeProperty ; rdfs:range ?r . }"
    ),
    Mapping.makeRuleMapping( // SUBCLASS
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 rdfs:subClassOf ?h . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x)", // Body
      "%s(x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // SUBPROPERTY
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 rdfs:subPropertyOf ?h . FILTER (!isBlank(?h) && !isBlank(?b1))}",
      "%s(x, y)", // Body
      "%s(x, y)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // EQUIVALENT-PROPERTY1
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 owl:equivalentProperty ?h . FILTER (!isBlank(?h) && !isBlank(?b1))}",
      "%s(x, y)", // Body
      "%s(x, y)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // EQUIVALENT-PROPERTY2
      "SELECT DISTINCT ?h ?b1 WHERE { ?h owl:equivalentProperty ?b1 . FILTER (!isBlank(?h) && !isBlank(?b1))}",
      "%s(x, y)", // Body
      "%s(x, y)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // RANGE
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 rdf:type owl:ObjectProperty ; rdfs:range ?h . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x, y)", // Body
      "%s(y)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // DOMAIN
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 rdfs:domain ?h . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x, y)", // Body
      "%s(x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // INVERSE1
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 owl:inverseOf ?h . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x, y)", // Body
      "%s(y, x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // INVERSE2
      "SELECT DISTINCT ?h ?b1 WHERE { ?h owl:inverseOf ?b1 . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x, y)", // Body
      "%s(y, x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // SYMMETRIC
      "SELECT DISTINCT ?h (?h AS ?b1) WHERE { ?h rdf:type owl:SymmetricProperty . FILTER (!isBlank(?h)) }",
      "%s(x, y)", // Body
      "%s(y, x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // REFLEXIVE
      "SELECT DISTINCT ?h ?b1 WHERE { ?h rdf:type owl:ReflexiveProperty ; rdfs:domain ?b1 . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x)", // Body
      "%s(x, x)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // TRANSITIVE
      "SELECT DISTINCT ?h (?h AS ?b1) (?h AS ?b2) WHERE { ?h rdf:type owl:TransitiveProperty . FILTER(!isBlank(?h)) }",
      "%s(x, y), %s(y, z)", // Body
      "%s(x, z)", // Head
      backwards
    ),
    Mapping.makeRuleMapping( // INTERSECTION1
      "SELECT DISTINCT ?h ?b1 WHERE { ?b1 rdfs:subClassOf [ owl:intersectionOf/rdf:rest*/rdf:first ?h ] . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x)", // Body
      "%s(x)", // Head
      backwards
    ),
    Mapping( // INTERSECTION2
      "SELECT DISTINCT ?h ?b1 WHERE { [ owl:intersectionOf ?b1 ] rdfs:subClassOf ?h . FILTER (!isBlank(?h)) }",
      Mapping.toLoreRuleFunction(
        rdfList => {
          // Make body of form c0(x), c1(x), ..., c<M>(x)
          rdfList.as(classOf[RDFList]).asJavaList.asScala
            .map(c => Utils.toRelationName(c.asInstanceOf[Resource])) // Map r to relation name
            .map(c => s"$c(x)")
            .mkString(", ")
        },
        "%s", // Body
        "%s(x)", // Head
        backwards
      )
    ),
    Mapping.makeRuleMapping( // UNION
      "SELECT DISTINCT ?h ?b1 WHERE { [ owl:unionOf/rdf:rest*/rdf:first ?b1 ; rdfs:subClassOf ?h ] . FILTER (!isBlank(?h) && !isBlank(?b1)) }",
      "%s(x)", // Body
      "%s(x)", // Head
      backwards
    ),
    Mapping( // PropertyChain
      "SELECT DISTINCT ?h ?b1 WHERE { ?h owl:propertyChainAxiom ?b1 . FILTER(!isBlank(?h)) }",
      Mapping.toLoreRuleFunction(
        rdfList => {
          // Make body of form r0(x0, x1), r1(x1, x2), ..., r<M>(x<M>, xN)
          val rs = rdfList.as(classOf[RDFList]).asJavaList.asScala
          rs.zipWithIndex
            .map { case (r, i) =>
                  (Utils.toRelationName(r.asInstanceOf[Resource]), // Map r to relation name
                   i.toString,                                     // Map index to val
                   if (i == rs.length - 1) "N" else (i+1).toString // Next index, but N if last
                  ) 
                 }
            .map { case (r, v1, v2) => s"$r(x$v1, x$v2)" }
            .mkString(", ")
        },
        "%s",         // Body
        "%s(x0, xN)", // Head
        backwards
      )
    )
  )
}

