package org.leifhka.triplelore

import java.io.PrintStream
import java.util
import java.util.ArrayList
import java.util.concurrent.Callable
import java.sql.{Connection,DriverManager}

import picocli.CommandLine.Command
import picocli.CommandLine.IVersionProvider
import picocli.CommandLine.Option
import picocli.CommandLine.Parameters

import scala.collection.mutable.ListBuffer
import scala.jdk.CollectionConverters._
import scala.util.{Using, Try, Success, Failure}

import org.apache.jena.sys.JenaSystem;

@Command(
  name = "triplelore",
  version = Array("0.4.1"),
  descriptionHeading = "%n@|bold DESCRIPTION:|@%n",
  parameterListHeading = "%n@|bold PARAMETERS:|@%n",
  optionListHeading = "%n@|bold OPTIONS:|@%n",
  footerHeading = "%n",
  description = Array("TripleLore is a program for translating RDF-files into Lore-script that inserts the data into unary (classes) and binary (properties) relations, with RDFS and OWL axioms encoded via Lore-rules. The resulting Lore-script should be executed over a database using Lore. Triplelore can also create R2RML-mappings over the resulting database."),
  footer = Array("""@|bold EXAMPLES:|@

The following command translates the RDF-files ex_data.ttl and ex_ont.ttl to a Lore-script ex.lore:

  java -jar triplelore.jar -o ex.lore ex_ont.ttl ex_data.ttl 

The following command does the same as above, but creates backward chaining rules instead of the default forward chaning rules:

  java -jar triplelore.jar -b -o ex.lore ex_ont.ttl ex_data.ttl 

The following command creates a file with R2RML-mappings of all triples in the database (to be used by e.g. Ontop):

  java -jar triplelore.jar -m r2rml -d mydb -U myuser -o mappings.ttl

@|bold FURTHER INFORMATION:|@

Webpage: https://leifhka.org/lore/triplelore
Source: https://gitlab.com/leifhka/triplelore
Downloads: https://gitlab.com/leifhka/triplelore/-/releases
Issues: Please report any issues at
        https://gitlab.com/leifhka/triplelore/-/issues
"""
  )
)
class TripleLore extends Callable[Int] {

  @Option(
    names = Array("-V", "--version"), versionHelp = true,
    description = Array("Display version info.")
  )
  var versionInfoRequested: Boolean = false;
  
  @Option(
    names = Array("--help"), usageHelp = true,
    description = Array("Display this help message.")
  )
  var usageHelpRequested: Boolean = false;

  @Option(
    names = Array("-v", "--verbose"),
    description = Array("Verbose output. Will display information about what the execution is doing.")
  )
  var verbose: Boolean = false;

  @Option(
    names = Array("-b", "--backwards"),
    description = Array("Use backwards chaining rules, instead of forward chaining, when writing to Lore.")
  )
  var backward: Boolean = false;

  @Option(
    names = Array("-sc", "--skipCheck"),
    description = Array("Skip checking model for correctness.")
  )
  var skipCheck: Boolean = false;

  @Parameters(
    description = Array("The input file path(s) to act on.")
  )
  val inputFiles: java.util.List[String] = new java.util.LinkedList[String]();

  @Option(
    names = Array("-o", "--output"),
    description = Array("The output file path to write output to.")
  )
  var output: String = null;

  @Option(
    names = Array("-d", "--database"),
    description = Array("Which database to connect to. (default: ${DEFAULT-VALUE})")
  )
  var database: String = null;

  @Option(
    names = Array("-h", "--host"),
    description = Array("The host containing the database to connect to. "
                   + "(default: ${DEFAULT-VALUE})")
  )
  var host: String = "localhost";

  @Option(
    names = Array("-U", "--user"),
    description = Array("The username of the user to log in as.")
  )
  var username: String = null;

  @Option(
    names = Array("-P", "--password"),
    description = Array("The password of the user to log in as. (Will be prompted for if not provided and needed.)")
  )
  var password: String = null;

  @Option(
    names = Array("-m", "--mode"),
    description = Array("The mode of operation to be applied to input.%n"
                   + "(legal values: rdftolore, r2rml, directmappings); "
                   + "default: ${DEFAULT-VALUE})")
  )
  var mode: String = "rdftolore";

  override def call(): Int = {

    JenaSystem.init()

    if (!checkArgs()) {
      1
    } else if (this.mode == "r2rml") {
      executeMappingMode()
    } else if (this.mode == "rdftolore") {
      executeRdfToLore()
    } else if (this.mode == "directmappings") {
      executeDirectMappings()
    }
    0 // never happens
  }

  def executeRdfToLore(): Int = {

    Using(if (output != null) new PrintStream(output) else System.out) { out =>
      LoreWriter.transformWriteTo(this.inputFiles.asScala.toList, skipCheck, backward, out)
    } match {
      case Success(_) => 0
      case Failure(e) => {
        error(s"Error when making Lore-script: ${e.getMessage()}.")
        1
      }
    }
  }

  def executeMappingMode(): Int = {

    var conn: Connection = null

    Using(if (output != null) new PrintStream(output) else System.out) { out =>
      try {
        conn = connect()
          R2RMLGenerator.execute(out, conn)
      } catch {
        case e: Throwable => {
          error("Error when making mappings: " + e.getMessage())
          return 1
        }
      }
      if (conn != null) conn.close()
    } match {
      case Success(_) => 0
      case Failure(e) => {
        error(s"Error when making mappings: ${e.getMessage()}.")
        1
      }
    }
  }

  def executeDirectMappings(): Int = {

    var conn: Connection = null

    try {
      conn = connect()
      val mappings = DirectMappings.makeMappings(conn)
      val mappingsScript = mappings.get.mkString("\n")
      val res = Using(if (output != null) new PrintStream(output) else System.out) {
        out => out.println(mappingsScript);
      }
      res.get
    } catch {
      case e: Throwable => {
        error("Error when making direct mappings: " + e.getMessage())
        return 1
      }
    }
    if (conn != null) conn.close()

    0
  }

  def connect(): Connection = {

    Class.forName("org.postgresql.Driver");
    // Create a connection to the database
    val password = getPassword()
    var connectionStr = "user=" + this.username
    if (!password.equals("")) {
      connectionStr += "&password=" + password
    }
    connectionStr += "&port=5432"

    val jdbcUrl = "jdbc:postgresql://" + this.host + "/" + this.database + "?" + connectionStr

    DriverManager.getConnection(jdbcUrl)
  }

  def getPassword(): String = {

    if (this.password != null) {
      this.password
    } else {
      new String(System.console().readPassword("Password for " + this.username + " on " + this.database + ": "))
    }
  }

  def checkArgs(): Boolean = {

    if (this.mode.equals("r2rml")) {
      if (this.database == null) {
        error("No database provided.")
        return false
      } else if (this.username == null) {
        error("No database username provided.")
        return false
      }
    } else if (this.mode.equals("rdftolore")) {
      if (this.inputFiles.isEmpty) {
        error("No input files provided.")
        return false
      }
    } else if (this.mode.equals("directmappings")) {

    } else {
      error(s"Unknown mode: ${this.mode}")
      return false
    }
    return true
  }

  def error(msg: String): Unit = 
    System.err.println(s"[ERROR] $msg See --help for usage.")
}
